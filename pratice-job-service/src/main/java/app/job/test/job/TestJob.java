package app.job.test.job;

import core.framework.scheduler.Job;
import core.framework.scheduler.JobContext;

import java.time.ZonedDateTime;

public class TestJob implements Job {

    @Override
    public void execute(JobContext context) throws Exception {
        System.out.println("------------job start-------------");
        System.out.println(ZonedDateTime.now());
        System.out.println(Thread.currentThread().getName());
        Thread.sleep(3000);
        System.out.println("------------job finish------------");
    }
}
