package app;

import core.framework.module.App;

public class PraticeExecutorApp extends App {
    @Override
    protected void initialize() {
        http().httpPort(8081);
        load(new TestModule());
    }
}
