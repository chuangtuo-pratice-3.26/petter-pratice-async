package app.publish.api.publish;

import core.framework.api.json.Property;

public class TestMessage {

    @Property(name = "test_id")
    public Integer testId;

    @Property(name = "test_name")
    public String testName;
}
