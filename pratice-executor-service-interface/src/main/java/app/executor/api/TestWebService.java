package app.executor.api;

import app.executor.api.executor.TestResponse;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.Path;

public interface TestWebService {

    @GET
    @Path("/executor/test")
    TestResponse testExecutor();
}
