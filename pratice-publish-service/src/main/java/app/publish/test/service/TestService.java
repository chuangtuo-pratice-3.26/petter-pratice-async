package app.publish.test.service;

import app.publish.api.publish.TestMessage;
import app.publish.api.publish.TestResponse;
import core.framework.inject.Inject;
import core.framework.kafka.MessagePublisher;

public class TestService {
    @Inject
    private MessagePublisher<TestMessage> publisher;

    public TestResponse publishTest() {
        TestMessage testMessage = new TestMessage();
        testMessage.testId = 12345;
        testMessage.testName = "cxk";
        publisher.publish("test-key", testMessage);
        TestResponse testResponse = new TestResponse();
        testResponse.b = Boolean.TRUE;
        return testResponse;
    }

    public TestResponse bulkPublish() {
        for (int i = 0; i < 100; i++) {
            TestMessage testMessage = new TestMessage();
            testMessage.testId = i;
            testMessage.testName = "cxk" + i;
            publisher.publish("bulk-test-key", testMessage);
        }
        TestResponse testResponse = new TestResponse();
        testResponse.b = Boolean.TRUE;
        return testResponse;
    }
}
