package app.executor.test.web;

import app.executor.api.TestWebService;
import app.executor.api.executor.TestResponse;
import app.executor.test.service.TestService;
import core.framework.inject.Inject;

public class TestWebServiceImpl implements TestWebService {
    @Inject
    private TestService testService;

    @Override
    public TestResponse testExecutor() {
        return testService.testExecutor();
    }
}
