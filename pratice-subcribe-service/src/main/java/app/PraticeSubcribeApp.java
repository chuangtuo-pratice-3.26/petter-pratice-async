package app;

import core.framework.module.App;

public class PraticeSubcribeApp extends App {
    @Override
    protected void initialize() {
        http().httpPort(9999);
        loadProperties("app.properties");
        load(new TestModule());
    }
}
