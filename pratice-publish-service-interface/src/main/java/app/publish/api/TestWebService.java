package app.publish.api;

import app.publish.api.publish.TestResponse;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.Path;

public interface TestWebService {

    @GET
    @Path("/publish/test")
    TestResponse publishTest();

    @GET
    @Path("/publish/bulk")
    TestResponse bulkPublish();
}
