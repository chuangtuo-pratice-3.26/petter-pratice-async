package app.subcribe.test.handler;

import app.publish.api.publish.TestMessage;
import core.framework.kafka.BulkMessageHandler;
import core.framework.kafka.Message;

import java.util.List;

public class TestBulkMessageHandler implements BulkMessageHandler<TestMessage> {
    @Override
    public void handle(List<Message<TestMessage>> messages) throws Exception {
        for (Message<TestMessage> msg : messages) {
            System.out.println("msg id = " + msg.value.testId);
            System.out.println("msg name = " + msg.value.testName);
        }
    }
}
