package app;

import core.framework.module.App;

public class PraticePublishApp extends App {
    @Override
    protected void initialize() {
        http().httpPort(8888);
        loadProperties("app.properties");
        load(new TestModule());
    }
}
