package app;

import core.framework.module.App;

public class PraticeJobApp extends App {
    @Override
    protected void initialize() {
        http().httpPort(7777);
        load(new TestModule());
    }
}
