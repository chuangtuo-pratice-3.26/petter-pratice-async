package app;

import app.job.test.job.TestJob;
import core.framework.module.Module;

import java.time.Duration;

public class TestModule extends Module {

    @Override
    protected void initialize() {
        schedule().fixedRate("test-job", new TestJob(), Duration.ofSeconds(5));
    }
}
