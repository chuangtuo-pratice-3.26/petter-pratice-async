package app;

import app.publish.api.publish.TestMessage;
import app.subcribe.test.handler.TestBulkMessageHandler;
import app.subcribe.test.handler.TestMessageHandler;
import core.framework.module.Module;

public class TestModule extends Module {
    @Override
    protected void initialize() {
        kafka().uri(requiredProperty("app.kafka.uri"));
//        kafka().subscribe("test-topic", TestMessage.class, bind(TestMessageHandler.class));
        kafka().subscribe("test-topic", TestMessage.class, bind(TestBulkMessageHandler.class));
    }
}
