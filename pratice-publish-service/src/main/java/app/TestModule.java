package app;

import app.publish.api.TestWebService;
import app.publish.api.publish.TestMessage;
import app.publish.test.service.TestService;
import app.publish.test.web.TestWebServiceImpl;
import core.framework.module.Module;

public class TestModule extends Module {
    @Override
    protected void initialize() {
        kafka().uri(requiredProperty("app.kafka.uri"));
        kafka().publish("test-topic", TestMessage.class);

        bind(TestService.class);
        api().service(TestWebService.class, bind(TestWebServiceImpl.class));
    }
}
