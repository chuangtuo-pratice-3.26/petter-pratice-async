package app.executor.test.service;

import app.executor.api.executor.TestResponse;
import core.framework.async.Executor;
import core.framework.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestService {
    private final Logger logger = LoggerFactory.getLogger(TestService.class);
    @Inject
    private Executor executor;

    public TestResponse testExecutor() {

        System.out.println("----------------------------------start------------------------------------");
        System.out.println(Thread.currentThread().getName());

        executor.submit("test-executor", () -> {

            System.out.println("------------------------------async start-----------------------------------");
            System.out.println(Thread.currentThread().getName());
            Thread.sleep(10000);
            System.out.println("------------------------------async finish-----------------------------------");
        });

        System.out.println("------------------------------return-----------------------------------");

        TestResponse response = new TestResponse();
        response.b = true;
        return response;
    }
}
