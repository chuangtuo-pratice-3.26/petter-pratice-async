package app.subcribe.test.handler;

import app.publish.api.publish.TestMessage;
import core.framework.kafka.MessageHandler;

public class TestMessageHandler implements MessageHandler<TestMessage> {

    @Override
    public void handle(String key, TestMessage value) throws Exception {
        System.out.println("handler start-----------------------------------");
        System.out.println("the key is " + key);
        System.out.println("the value id is " + value.testId);
        System.out.println("the value name is " + value.testName);
    }
}
