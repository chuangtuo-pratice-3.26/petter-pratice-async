package app;

import app.executor.api.TestWebService;
import app.executor.test.service.TestService;
import app.executor.test.web.TestWebServiceImpl;
import core.framework.module.Module;

public class TestModule extends Module {
    @Override
    protected void initialize() {
        executor().add();
        bind(TestService.class);
        api().service(TestWebService.class, bind(TestWebServiceImpl.class));
    }
}
