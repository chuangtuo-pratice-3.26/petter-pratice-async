package app.publish.test.web;

import app.publish.api.TestWebService;
import app.publish.api.publish.TestResponse;
import app.publish.test.service.TestService;
import core.framework.inject.Inject;

public class TestWebServiceImpl implements TestWebService {
    @Inject
    private TestService testService;

    @Override
    public TestResponse publishTest() {
        return testService.publishTest();
    }

    @Override
    public TestResponse bulkPublish() {
        return testService.bulkPublish();
    }
}
